= Programació en bases de dades
:doctype: article
:encoding: utf-8
:lang: ca
:toc: left
:toclevels: 3
:numbered:
:ascii-ids:

<<<

== Activitat cursors

=== Solució

1. Escriu una funció que rebi el codi d'un país i mostri el nom dels idiomes que
s'hi parlen, indicant per a cadascun si és oficial o no. Utilitza un bucle
`WHILE`.
+
[source,sql]
----
CREATE OR REPLACE FUNCTION show_langs(countrycode char) RETURNS void AS $$
DECLARE
  curs CURSOR FOR SELECT language, isofficial FROM countrylanguage cl
    WHERE cl.countrycode=show_langs.countrycode;
  lang text;
  official boolean;
BEGIN
  OPEN curs;
  FETCH curs INTO lang, official;
  WHILE FOUND LOOP
    RAISE NOTICE 'Idioma: %, és oficial: %.',lang, official;
    FETCH curs INTO lang, official;
  END LOOP;
  CLOSE curs;
END;
$$ LANGUAGE plpgsql;

select show_langs('ITA');
NOTICE:  Idioma: Italian, és oficial: t.
NOTICE:  Idioma: Sardinian, és oficial: f.
NOTICE:  Idioma: Friuli, és oficial: f.
NOTICE:  Idioma: French, és oficial: f.
NOTICE:  Idioma: German, és oficial: f.
NOTICE:  Idioma: Albaniana, és oficial: f.
NOTICE:  Idioma: Slovene, és oficial: f.
NOTICE:  Idioma: Romani, és oficial: f.
 show_langs
------------

(1 row)
----

2. Repeteix l'exercici anterior, però utilitzant un bucle `FOR`.
+
[source,sql]
----
CREATE OR REPLACE FUNCTION show_langs(countrycode char) RETURNS void AS $$
DECLARE
  curs CURSOR FOR SELECT language, isofficial FROM countrylanguage cl
    WHERE cl.countrycode=show_langs.countrycode;
BEGIN
  FOR result IN curs LOOP
    RAISE NOTICE 'Idioma: %, és oficial: %.',result.language, result.isofficial;
  END LOOP;
END;
$$ LANGUAGE plpgsql;

select show_langs('ITA');
NOTICE:  Idioma: Italian, és oficial: t.
NOTICE:  Idioma: Sardinian, és oficial: f.
NOTICE:  Idioma: Friuli, és oficial: f.
NOTICE:  Idioma: French, és oficial: f.
NOTICE:  Idioma: German, és oficial: f.
NOTICE:  Idioma: Albaniana, és oficial: f.
NOTICE:  Idioma: Slovene, és oficial: f.
NOTICE:  Idioma: Romani, és oficial: f.
 show_langs
------------

(1 row)
----

3. Fes una funció que mostri les ciutats d'un país. Es rep el codi del país per
paràmetre. Ha de mostrar les ciutats ordenades alfabèticament, i numerant cada
línia.
+
[source,sql]
----
CREATE OR REPLACE FUNCTION show_cities(countrycode char) RETURNS void AS $$
DECLARE
  curs CURSOR FOR SELECT name FROM city
    WHERE city.countrycode=show_cities.countrycode
    ORDER BY name;
  nline int := 1;
BEGIN
  FOR result IN curs LOOP
    RAISE NOTICE '%- %',nline,result.name;
    nline:=nline+1;
  END LOOP;
END;
$$ LANGUAGE plpgsql;

select show_cities('ESP');
NOTICE:  1- A Coruña (La Coruña)
NOTICE:  2- Albacete
NOTICE:  3- Alcalá de Henares
NOTICE:  4- Alcorcón
NOTICE:  5- Algeciras
----

4. Modifica l'exercici anterior per fer que totes les ciutats del país es
retornin en un conjunt, però que se'n mostrin per pantalla un màxim de 5. Si
n'hi ha més, al final de la llista es mostraran uns punts suspensius.
+
[source,sql]
----
CREATE OR REPLACE FUNCTION get_cities(countrycode char) RETURNS SETOF city AS $$
DECLARE
  curs CURSOR FOR SELECT * FROM city
    WHERE city.countrycode=get_cities.countrycode
    ORDER BY name;
  nline int := 1;
BEGIN
  FOR result IN curs LOOP
    IF nline < 6 THEN
      RAISE NOTICE '%- %',nline,result.name;
    ELSIF nline = 6 THEN
      RAISE NOTICE '...';
    END IF;
    nline:=nline+1;
    RETURN NEXT result;
  END LOOP;
END;
$$ LANGUAGE plpgsql;

select get_cities('ESP');
NOTICE:  1- A Coruña (La Coruña)
NOTICE:  2- Albacete
NOTICE:  3- Alcalá de Henares
NOTICE:  4- Alcorcón
NOTICE:  5- Algeciras
NOTICE:  ...
----

5. Fes una funció que rebi un nombre enter. La funció agafarà tots els noms de
país ordenats alfabèticament i en retornarà només aquells que ocupen una
posició múltiple del nombre rebut. Per exemple, si el nombre és 3, retornarà el
tercer nom, el sisè, el novè, etc.
+
[source,sql]
----
CREATE OR REPLACE FUNCTION show_some_countries(n int) RETURNS SETOF country AS $$
DECLARE
  curs CURSOR FOR SELECT * FROM country ORDER BY name;
  country country;
BEGIN
  OPEN curs;
  FETCH RELATIVE n FROM curs INTO country;
  WHILE FOUND LOOP
    RETURN NEXT country;
    FETCH RELATIVE n FROM curs INTO country;
  END LOOP;
  CLOSE curs;
END;
$$ LANGUAGE plpgsql;

select name from show_some_countries(8);
name
-------------------------
Antarctica
Bahamas
Bermuda
Brunei
Cayman Islands
Comoros
...
----

6. Fes una funció que calculi quants països d'una regió donada tenen una
esperança de vida superior a la mitjana del seu continent. La funció ha
de retornar NULL si la regió no existeix.
+
[source,sql]
----
CREATE OR REPLACE FUNCTION best_lifeexpectancy(regionname text) RETURNS int AS $$
DECLARE
  curs CURSOR FOR SELECT * FROM country WHERE region=regionname;
  count int := 0;
  avglifeexp real;
BEGIN
  SELECT AVG(lifeexpectancy) INTO STRICT avglifeexp
    FROM country c
    WHERE continent=(
      SELECT continent FROM country WHERE region=regionname LIMIT 1
    );
  IF avglifeexp IS NULL THEN
    RETURN NULL;
  END IF;
  FOR country IN curs LOOP
    IF country.lifeexpectancy > avglifeexp THEN
      count=count+1;
    END IF;
  END LOOP;
  RETURN count;
END;
$$ LANGUAGE plpgsql;

select best_lifeexpectancy('Western Europe');
 best_lifeexpectancy
---------------------
                   9
(1 row)
----

7. Fes una funció que, utilitzant la funció de l'exercici anterior, mostri per
pantalla el nom de les regions on menys de la meitat dels països tenen una
esperança de vida superior a la mitjana del seu continent.
+
[source,sql]
----
CREATE OR REPLACE FUNCTION poor_regions() RETURNS void AS $$
DECLARE
  poor_regions_curs CURSOR FOR SELECT DISTINCT region FROM country;
  ncountries int;
  nrichcountries int;
BEGIN
  -- Per cada regió
  FOR result IN poor_regions_curs LOOP
    -- Número de països de la regió
    SELECT COUNT(*) INTO STRICT ncountries FROM country WHERE region=result.region;
    -- Número de països amb esperança de vida superior a la mitjana
    SELECT best_lifeexpectancy(result.region) INTO STRICT nrichcountries;
    -- Comprovem condició
    IF ncountries > nrichcountries*2 THEN
      RAISE NOTICE '%',result.region;
    END IF;
  END LOOP;
END;
$$ LANGUAGE plpgsql;

select poor_regions();
NOTICE:  Micronesia/Caribbean
NOTICE:  Southern Africa
NOTICE:  Australia and New Zealand
NOTICE:  Central Africa
NOTICE:  Micronesia
NOTICE:  Polynesia
NOTICE:  Western Africa
NOTICE:  Melanesia
NOTICE:  Eastern Europe
NOTICE:  Eastern Africa
NOTICE:  Southern and Central Asia
NOTICE:  Central America
NOTICE:  Baltic Countries
 poor_regions
--------------

(1 row)
----

8. Escriu una funció que rebi el nom d'una regió i el percentatge d'increment
del producte interior brut (`gnp`). La funció incrementarà el PIB de cada país
de la regió en el tant per cent indicat i retornarà la quantitat de països
modificats. Per a cada país es mostrarà per pantalla el seu codi i nom, i el PIB
abans i després de la modificació. El percentatge d'increment es rebrà en tant
per ú, és a dir, per exemple per un 10% es rebria 0,1.
+
[source,sql]
----
CREATE OR REPLACE FUNCTION increase_gnp(regionname text, growth real) RETURNS int AS $$
DECLARE
  curs CURSOR FOR SELECT * FROM country WHERE region=regionname;
  ncountries int := 0;
  oldgnp real;
  newgnp real;
BEGIN
  FOR country IN curs LOOP
    ncountries:=ncountries+1;
    oldgnp:=country.gnp;
    UPDATE country SET gnp=gnp+gnp*growth WHERE CURRENT OF curs RETURNING gnp INTO newgnp;
    RAISE NOTICE 'Codi: %, Nom: %, PIB abans: %, PIB després: %',
      country.code,country.name,oldgnp,newgnp;
  END LOOP;
  RETURN ncountries;
END;
$$ LANGUAGE plpgsql;

select increase_gnp('Southern and Central Asia', 0.05);
NOTICE:  Codi: AFG, Nom: Afghanistan, PIB abans: 5976, PIB després: 6274.8
NOTICE:  Codi: BGD, Nom: Bangladesh, PIB abans: 32852, PIB després: 34494.6
NOTICE:  Codi: BTN, Nom: Bhutan, PIB abans: 372, PIB després: 390.6
NOTICE:  Codi: IND, Nom: India, PIB abans: 447114, PIB després: 469470
NOTICE:  Codi: IRN, Nom: Iran, PIB abans: 195746, PIB després: 205533
NOTICE:  Codi: KAZ, Nom: Kazakstan, PIB abans: 24375, PIB després: 25593.8
NOTICE:  Codi: KGZ, Nom: Kyrgyzstan, PIB abans: 1626, PIB després: 1707.3
NOTICE:  Codi: MDV, Nom: Maldives, PIB abans: 199, PIB després: 208.95
NOTICE:  Codi: NPL, Nom: Nepal, PIB abans: 4768, PIB després: 5006.4
NOTICE:  Codi: PAK, Nom: Pakistan, PIB abans: 61289, PIB després: 64353.4
NOTICE:  Codi: LKA, Nom: Sri Lanka, PIB abans: 15706, PIB després: 16491.3
NOTICE:  Codi: TJK, Nom: Tajikistan, PIB abans: 1990, PIB després: 2089.5
NOTICE:  Codi: TKM, Nom: Turkmenistan, PIB abans: 4397, PIB després: 4616.85
NOTICE:  Codi: UZB, Nom: Uzbekistan, PIB abans: 14194, PIB després: 14903.7
 increase_gnp
--------------
           14
(1 row)
----

9. Escriu una funció que rebi un nombre enter positiu, _n_. La funció repetirà
la següent operació _n_ cops. A cada iteració, ha d'agafar el país amb el PIB
més alt i restar-li un 10%. Aquesta quantitat s'ha de repartir a parts iguals
entre els _n_ països més pobres, els que tenen un PIB més baix. S'ha de mostrar
per pantalla les modificacions que es van fent.
+
[source,sql]
----
CREATE OR REPLACE FUNCTION distribute_gnp(n int) RETURNS void AS $$
DECLARE
  curs CURSOR FOR SELECT * FROM country ORDER BY gnp LIMIT n;
  maxgnp real;
  currentgnp real;
  toshare real;
  richestcode char(3);
  richestname text;
BEGIN
  IF n<=0 THEN
    RAISE EXCEPTION 'S''ha de passar un nombre positiu.';
  END IF;
  FOR i IN 1..N LOOP
    RAISE NOTICE 'Iteració %',i;
    SELECT code, name, gnp INTO STRICT richestcode, richestname, maxgnp FROM country ORDER BY gnp DESC LIMIT 1;
    toshare:=maxgnp/10;
    UPDATE country SET gnp=gnp-toshare WHERE code=richestcode RETURNING gnp INTO currentgnp;
    RAISE NOTICE 'País més ric: % (%). Tenia %, es treu %, i es queda amb %.',richestname,richestcode,maxgnp,toshare,currentgnp;
    toshare:=toshare/n;
    FOR ccountry IN curs LOOP
      -- No podem utilitzar WHERE CURRENT OF perquè hem utilitzat ORDER BY a la consulta.
      UPDATE country SET gnp=gnp+toshare WHERE code=ccountry.code RETURNING gnp INTO currentgnp;
      RAISE NOTICE 'Es dóna a % (%). Tenia %, se li dóna %, i es queda amb %.',ccountry.name,ccountry.code,ccountry.gnp,toshare,currentgnp;
    END LOOP;
  END LOOP;
END;
$$ LANGUAGE plpgsql;

select distribute_gnp(4);
NOTICE:  Iteració 1
NOTICE:  País més ric: United States (USA). Tenia 8.5107e+06, es treu 851070, i es queda amb 7.65963e+06.
NOTICE:  Es dóna a Falkland Islands (FLK). Tenia 0.00, se li dóna 212768, i es queda amb 212768.
NOTICE:  Es dóna a Faroe Islands (FRO). Tenia 0.00, se li dóna 212768, i es queda amb 212768.
NOTICE:  Es dóna a Svalbard and Jan Mayen (SJM). Tenia 0.00, se li dóna 212768, i es queda amb 212768.
NOTICE:  Es dóna a Greenland (GRL). Tenia 0.00, se li dóna 212768, i es queda amb 212768.
NOTICE:  Iteració 2
NOTICE:  País més ric: United States (USA). Tenia 7.65963e+06, es treu 765963, i es queda amb 6.89367e+06.
NOTICE:  Es dóna a East Timor (TMP). Tenia 0.00, se li dóna 191491, i es queda amb 191491.
NOTICE:  Es dóna a Christmas Island (CXR). Tenia 0.00, se li dóna 191491, i es queda amb 191491.
NOTICE:  Es dóna a Mayotte (MYT). Tenia 0.00, se li dóna 191491, i es queda amb 191491.
NOTICE:  Es dóna a Cocos (Keeling) Islands (CCK). Tenia 0.00, se li dóna 191491, i es queda amb 191491.
NOTICE:  Iteració 3
NOTICE:  País més ric: United States (USA). Tenia 6.89367e+06, es treu 689367, i es queda amb 6.2043e+06.
NOTICE:  Es dóna a Niue (NIU). Tenia 0.00, se li dóna 172342, i es queda amb 172342.
NOTICE:  Es dóna a Norfolk Island (NFK). Tenia 0.00, se li dóna 172342, i es queda amb 172342.
NOTICE:  Es dóna a Northern Mariana Islands (MNP). Tenia 0.00, se li dóna 172342, i es queda amb 172342.
NOTICE:  Es dóna a Pitcairn (PCN). Tenia 0.00, se li dóna 172342, i es queda amb 172342.
NOTICE:  Iteració 4
NOTICE:  País més ric: United States (USA). Tenia 6.2043e+06, es treu 620430, i es queda amb 5.58387e+06.
NOTICE:  Es dóna a Saint Helena (SHN). Tenia 0.00, se li dóna 155108, i es queda amb 155108.
NOTICE:  Es dóna a Saint Pierre and Miquelon (SPM). Tenia 0.00, se li dóna 155108, i es queda amb 155108.
NOTICE:  Es dóna a Wallis and Futuna (WLF). Tenia 0.00, se li dóna 155108, i es queda amb 155108.
NOTICE:  Es dóna a Tokelau (TKL). Tenia 0.00, se li dóna 155108, i es queda amb 155108.
 distribute_gnp
----------------

(1 row)


-- Alternativa amb FOR UPDATE:

CREATE OR REPLACE FUNCTION distribute_gnp(n int) RETURNS void AS $$
DECLARE
  curs CURSOR FOR SELECT * FROM country ORDER BY gnp LIMIT n FOR UPDATE;
  maxgnp real;
  currentgnp real;
  toshare real;
  richestcode char(3);
  richestname text;
BEGIN
  IF n<=0 THEN
    RAISE EXCEPTION 'S''ha de passar un nombre positiu.';
  END IF;
  FOR i IN 1..N LOOP
    RAISE NOTICE 'Iteració %',i;
    SELECT code, name, gnp INTO STRICT richestcode, richestname, maxgnp FROM country ORDER BY gnp DESC LIMIT 1;
    toshare:=maxgnp/10;
    UPDATE country SET gnp=gnp-toshare WHERE code=richestcode RETURNING gnp INTO currentgnp;
    RAISE NOTICE 'País més ric: % (%). Tenia %, es treu %, i es queda amb %.',richestname,richestcode,maxgnp,toshare,currentgnp;
    toshare:=toshare/n;
    FOR ccountry IN curs LOOP
      UPDATE country SET gnp=gnp+toshare WHERE CURRENT OF curs RETURNING gnp INTO currentgnp;
      RAISE NOTICE 'Es dóna a % (%). Tenia %, se li dóna %, i es queda amb %.',ccountry.name,ccountry.code,ccountry.gnp,toshare,currentgnp;
    END LOOP;
  END LOOP;
END;
$$ LANGUAGE plpgsql;
----

10. Fes una funció que apugi el PIB de tots els països que el tinguin per sota
de la mitjana dels països de la seva regió. La pujada serà d'un 30% de la
diferència entre el seu PIB i la mitjana de la regió.
+
[source,sql]
----
CREATE OR REPLACE FUNCTION increase_gnp() RETURNS void AS $$
DECLARE
  regioncursor CURSOR FOR SELECT DISTINCT region FROM country;
  avggnp real;
  countrycursor CURSOR (currentregion text) FOR SELECT * FROM country WHERE region=currentregion AND gnp<avggnp;
BEGIN
  FOR currentregion IN regioncursor LOOP
    SELECT AVG(gnp) INTO STRICT avggnp FROM country WHERE region=currentregion.region;
    RAISE NOTICE 'Regió: %, PIB mitjà: %',currentregion.region,avggnp;
    FOR currentcountry IN countrycursor(currentregion.region) LOOP
      RAISE NOTICE 'País: %, PIB inicial: %, PIB final: %',
        currentcountry.name,currentcountry.gnp,currentcountry.gnp+0.3*(avggnp-currentcountry.gnp);
      UPDATE country SET gnp=gnp+0.3*(avggnp-gnp) WHERE CURRENT OF countrycursor;
    END LOOP;
  END LOOP;
END;
$$ LANGUAGE plpgsql;

select increase_gnp();
NOTICE:  Regió: Micronesia/Caribbean, PIB mitjà: 212768
NOTICE:  Regió: Southern Africa, PIB mitjà: 25386.2
NOTICE:  País: Botswana, PIB inicial: 4834.00, PIB final: 10999.659765625
NOTICE:  País: Lesotho, PIB inicial: 1061.00, PIB final: 8358.559765625
NOTICE:  País: Namibia, PIB inicial: 3101.00, PIB final: 9786.559765625
NOTICE:  País: Swaziland, PIB inicial: 1206.00, PIB final: 8460.059765625
NOTICE:  Regió: Australia and New Zealand, PIB mitjà: 165238
NOTICE:  País: Christmas Island, PIB inicial: 139596.75, PIB final: 147289.265625
NOTICE:  País: Norfolk Island, PIB inicial: 125637.08, PIB final: 137517.496625
NOTICE:  País: New Zealand, PIB inicial: 54669.00, PIB final: 87839.840625
NOTICE:  País: Cocos (Keeling) Islands, PIB inicial: 155107.52, PIB final: 158146.804625
...
----
