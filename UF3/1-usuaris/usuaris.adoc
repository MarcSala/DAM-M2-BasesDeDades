= Llenguatges SQL: DCL
:doctype: article
:encoding: utf-8
:lang: ca
:toc: left
:toclevels: 3
:numbered:
:ascii-ids:

<<<

== Introducció a PostgreSQL

Instal·lació de PostgreSQL en Debian:

----
# apt-get update
# apt-get install postgresql
----

Primer accés i configuració bàsica:

----
# su - postgres
$ createdb testdb --owner=postgres
$ dropdb testdb
$ psql -l
                                  List of databases
   Name    |  Owner   | Encoding |   Collate   |    Ctype    |   Access privileges
-----------+----------+----------+-------------+-------------+-----------------------
 postgres  | postgres | UTF8     | ca_ES.UTF-8 | ca_ES.UTF-8 |
 template0 | postgres | UTF8     | ca_ES.UTF-8 | ca_ES.UTF-8 | =c/postgres          +
           |          |          |             |             | postgres=CTc/postgres
 template1 | postgres | UTF8     | ca_ES.UTF-8 | ca_ES.UTF-8 | =c/postgres          +
           |          |          |             |             | postgres=CTc/postgres
(3 rows)
$ createdb testdb --owner=postgres
postgres@sequoia:~$ psql -d testdb
psql (9.6.6)
Type "help" for help.

testdb=# \q
----

Opcions bàsiques de `psql`:

- `-d`: base de dades a utilitzar.
- `-h`: nom de host/IP on connectar-se.
- `-U`: usuari amb què establir la connexió.

Ordres de gestió bàsiques dins del `psql`:

[options="header"]
|====
|Ordre |Com recordar-ho |Descripció |Equivalent en mysql
|`\d` |DESCRIBE |Mostra les taules, vistes, etc. de la BD actual |`SHOW TABLES`
|`\l` |LIST |Mostra les BD que hi ha |`SHOW DATABASES`
|`\i <fitxer>` |INCLUDE |Executa un script SQL des d'un fitxer |`SOURCE <fitxer>`
|`\c <DB>` |CONNECT |Activar una base de dades |`USE <DB>`
|`\d <taula>` |DESCRIBE |Mostra l'estructura d'una taula |`DESC <taula>`
|`\q` |QUIT |Sortir de l'entorn |`EXIT`
|`\x` |EXPANDED |Activa o desactiva la visualització estesa |`\G`
|====

Variants de l'ordre `\d` per examinar el contingut de la base de dades:

[options="header"]
|====
|Ordre |Descripció
|`\dt` |Mostra les taules
|`\di` |Mostra els índexs
|`\dv` |Mostra les vistes
|`\ds` |Mostra les seqüències
|`\dp` |Mostra els privilegis
|`\df` |Mostra les funcions
|`\du` |Mostra els rols (usuaris)
|`\dn` |Mostra els esquemes
|`\dn+` |Mostra els esquemes, incloent-hi els privilegis que s'hi han definit
|====

Podem utilitzar comodins per veure només algunes de les taules o privilegis.

Per exemple:

====
- `\dt nomesquema.*` mostra les taules de l'esquema `nomesquema`.
- `\dt \*.*` mostra totes les taules de la base de dades.
- `\d nomesquema.nomtaula` mostra les columnes i tipus de la taula seleccionada.
====

=== Usuari de treball

Com a usuari `postgres` creem un usuari *el nom del qual coincideixi amb el
nom del nostre usuari del sistema operatiu*, i li assignem una base de dades:

----
$ createuser joan
$ createdb dbjoan --owner=joan
----

Després, podem connectar amb aquest usuari. No necessitem proporcionar una
contrasenya, perquè ja estem connectant amb l'usuari equivalent al sistema
operatiu:

----
$ psql -d dbjoan
psql (9.6.6)
Type "help" for help.

dbjoan=> \du
                             List of roles
 Role name |                   Attributes                   | Member of
-----------+------------------------------------------------+-----------
 joan      |                                                | {}
 postgres  | Superuser, Create role, Create DB, Replication | {}

dbjoan=> create table tbl (a int);
CREATE TABLE
dbjoan=> \dt
       List of relations
 Schema | Name | Type  | Owner
--------+------+-------+-------
 public | tbl  | table | joan
(1 row)
----

Per utilitzar un client gràfic podem instal·lar el `pgadmin`:

----
# apt-get install pgadmin3
----

Per configurar el servidor només caldrà especificar el nostre nom d'usuari
(no cal posar ni host ni contrasenya, perquè utilitzem l'usuari del sistema
operatiu).

=== Importació de la base de dades `pagila`

- Obrim l'script `pagila-schema.sql` amb un editor de textos i reemplacem totes
les ocurrències de `postgres` pel nom del nostre usuari (per exemple, `joan`).

- Connectem després com usuari `postgres` i importem la base de dades:

----
$ createdb pagila --owner=joan
$ psql
postgres=# \c pagila
pagila=# \i /ruta/a/pagila-schema.sql
pagila=# \i /ruta/a/pagila-data.sql
----

- Connectem amb el nostre usuari i comprovem les dades:

----
$ psql -d pagila
pagila=> select count(*) from film;
 count
-------
  1000
(1 row)
----

=== Usuari amb contrasenya

Si volem crear un usuari que utilitzi una contrasenya en comptes de ser un
usuari del sistema operatiu, haurem d'especificar-ho a l'ordre `createuser`:

----
$ createuser -P testuser
Enter password for new role:
Enter it again:
----

Després, com que per defecte s'utilitza l'autenticació automàtica, hem
d'especificar que volem connectar-nos a una adreça IP (igual que ho faríem si
el servidor fos remot) per tal que ens demani la contrasenya:

----
$ psql -U testuser -h 127.0.0.1 -d postgres
----

Aquí hem especificat que volem connectar a la base de dades `postgres` perquè
aquest usuari no té cap base de dades assignada en aquests moments.

De forma equivalent, també podem connectar-nos amb:

----
$ psql -h 127.0.0.1 postgres testuser
----

La base de dades `postgres` és una base de dades buida que es crea durant la
instal·lació i a la qual tots els usuaris tenen accés.

El fet que per usuaris locals s'intenta l'autenticació directament com usuari
del sistema operatiu, mentre que pels usuaris de xarxa s'intenti l'autenticació
amb contrasenya ve configurat per defecte al fitxer
`/etc/postgresql/9.6/main/pg_hba.conf`.

Aquest fitxer, si traiem totes les línies comentades amb #, queda així:

----
local   all             postgres                                peer
# TYPE  DATABASE        USER            ADDRESS                 METHOD
local   all             all                                     peer
host    all             all             127.0.0.1/32            md5
host    all             all             ::1/128                 md5
----

El significat de cadascuna d'aquestes línies és el següent:

1. L'usuari `postgres` utilitza sempre validació del sistema operatiu.
2. Les connexions locals utilitzen validació del sistema operatiu.
3. Les connexions de xarxa utilitzen validació per contrasenya. Només es
permeten connexions locals.
4. El mateix que l'anterior, per IPv6.

Aquest és el motiu pel qual necessitem establir una connexió de xarxa per tal
que el sistema ens demani una contrasenya.

Noteu també que, per defecte, l'accés remot al servidor està inhabilitat.

=== Usuari de gestió

No està recomanat utilitzar un usuari administrador com `postgres` per
treballar habitualment. Per això hem creat abans un usuari de treball.

A més, però, està recomanat tenir un usuari que pugui crear noves bases de
dades i crear nous rols, però que no sigui administrador:

----
$ createuser -d -r -P gestor
----

L'opció `-d` indica que l'usuari podrà crear bases de dades, i l'opció `-r`
que l'usuari podrà crear nous rols.

== Gestió d'usuaris

=== Usuaris i rols

El PostgreSQL gestiona els permisos d'accés a les bases de dades a partir del
concepte de rol. Un rol es pot pensar tant com un usuari, o com un grup
d'usuaris, depenent de com es configuri.

Els rols poden ser propietaris dels objectes d'una base de dades (com taules o
funcions) i poden assignar privilegis sobre aquests objectes a altres rols, per
tal de controlar qui té accés a quins objectes.

Addicionalment, és possible fer que un rol sigui membre d'un altre rol, cosa que
li permet utilitzar els privilegis assignats a aquest segon rol.

D'aquesta manera, el concepte de rol inclou els conceptes d'usuari i de grup en
un de sol, i proporciona una gran flexibilitat a l'hora de configurar els
permisos d'accés al nostre sistema.

=== Creació i destrucció de rols

Per crear un rol ho podem fer de dues formes:

- Amb l'ordre del sistema `createuser`.
- Amb l'ordre `CREATE ROLE` dins del `psql`.

Quan creem un rol podem especificar alguns privilegis globals:

- `LOGIN`: indica que es pot utilitzar aquest nom de rol per connectar-se al
PostgreSQL.

[TIP]
====
L'única diferència entre un usuari de la base de dades i un rol utilitzat com
un grup d'usuaris o com un conjunt de permisos és que l'usuari té assignat el
privilegi `LOGIN`.
====

[NOTE]
====
`CREATE USER` és idèntic a `CREATE ROLE` però pressuposa el privilegi `LOGIN`.
====

- `SUPERUSER`: s'utilitza per crear un administrador del SGBD. És el privilegi
que té l'usuari `postgres` que s'ha creat durant la instal·lació.

- `CREATEDB`: el rol té el privilegi de poder crear noves bases de dades.

- `CREATEROLE`: el rol pot crear, esborrar o modificar els rols.

- `REPLICATION`: es tracta d'un privilegi especial que s'utilitza quan tenim més
d'un servidor amb la mateixa base de dades (una rèplica). El rol que té aquest
permís (a més de LOGIN) però fer una rèplica de la base de dades del servidor
principal.

- `PASSWORD`: assigna una contrasenya d'accés per aquest rol.

[TIP]
====
`PASSWORD` només s'utilitza si l'usuari del PostgreSQL no es correspon amb un
usuari del sistema operatiu i necessita, per tant, un sistema d'autenticació.
====

.Exemple creació rol
====
El rol `nourol` podrà connectar-se al sistema (és un usuari), i crear noves
bases de dades. No es correspon amb un usuari del sistema operatiu, així que
li hem assignat una contrasenya:

----
=> CREATE ROLE nourol LOGIN CREATEDB PASSWORD '12345';
----

El mateix rol es pot crear amb `CREATE USER`, ja que té el privilegi `LOGIN`:

----
=> CREATE USER nourol CREATEDB PASSWORD '12345';
----

Igualment, el podríem haver creat des del terminal amb l'ordre:

----
$ createuser --login --createdb -P nourol
----

O, de forma equivalent:

----
$ createuser -l -d -P nourol
----
====

Per eliminar un rol podem utilitzar `DROP ROLE` des del PostgreSQL o `dropuser`
des del terminal.

====
Per eliminar el rol que hem creat a l'exemple anterior podem utilitzar:

----
=> DROP ROLE nourol;
----

O des del terminal:

----
$ dropuser nourol
----
====

=== Ús de rols com a grups d'usuaris

Si el nostre sistema té més d'uns quants usuaris, pot ser convenient agrupar
els privilegis en grups i atorgar als usuaris la pertinença a alguns d'aquests
grups.

Ja hem vist com crear els diferents rols, només cal tenir en compte que no
necessitem assignar-los el privilegi `LOGIN`.

Per assignar un usuari a un grup utilitzem l'ordre `GRANT`, i per treure'ls,
`REVOKE`.

.Ús de `GRANT`
====
Per incloure als usuaris `joan` i `maria` al grup `admins` faríem:

----
=> GRANT admins TO joan, maria;
----

Per treure l'usuari `joan` d'aquest grup:

----
=> REVOKE admins FROM joan;
----
====

[TIP]
====
Hem parlat de grups d'usuaris, però res impedeix que un rol agrupi altres rols
que no siguin usuaris, de manera que podem desgranar els privilegis en tants
grups i subgrups com vulguem.
====

=== Herència de privilegis

Per defecte, un rol hereta automàticament tots els privilegis dels grups als
quals pertany.

.Herència de privilegis
====
Això significa que si, per exemple, el rol `personal` té accés de lectura a la
taula `nomines`, i l'usuari `pere` pertany al grup `personal`, automàticament
l'usuari `pere` podrà llegir la taula `nomines`.
====

Aquest comportament es pot canviar afegint el modificador `NOINHERIT` a la
creació del rol (per defecte se suposa `INHERIT`).

En aquest cas, l'usuari no rep automàticament els privilegis del grup, sinó que
ha d'adoptar el rol primer, utilitzant `SET ROLE`.

.Sense herència de privilegis
====
Seguint l'exemple anterior, l'usuari `pere` podrà accedir a la taula
`nomines` només d'executar l'ordre:

----
=> SET ROLE personal;
----

Llavors, en `pere` adoptarà tots els privilegis del rol `personal` i deixarà de
tenir els privilegis que tingués com a rol `pere`.
====

[TIP]
====
Val la pena notar que amb `NOINHERIT`, els rols del PostgreSQL funcionen igual
que els usuaris de GNU/Linux. L'ordre `SET ROLE` fa el paper de `su`.
====

[NOTE]
====
Per tornar al seu rol habitual, un usuari pot utilitzar l'ordre `SET ROLE NONE`
o `RESET ROLE`.
====

.Com funciona l'herència de privilegis
====
Imaginem que hem creat els següents rols:

----
=> CREATE ROLE pere LOGIN INHERIT;
=> CREATE ROLE admins NOINHERIT;
=> CREATE ROLE personal NOINHERIT;
=> GRANT admins TO pere;
=> GRANT personal TO admins;
----

Quan es connecta, el `pere` té automàticament tots els seus privilegis, més
els privilegis que tingui el rol `admins`, perquè hem configurat el rol `pere`
de manera que hereti els permisos.

El `pere` també té, indirectament, el rol `personal`, però per poder fer-ne ús
ho ha de declarar explícitament amb `SET ROLE personal`, perquè hem configurat
el rol `admins` sense herència.
====

[NOTE]
====
Podem utilitzar `SELECT SESSION_USER, CURRENT_USER;` per veure amb quin usuari
hem connectat i en quin rol estem actualment.
====

[WARNING]
====
Els atributs `LOGIN`, `SUPERUSER`, `CREATEDB` i `CREATEROLE` són especials en
el sentit que no s'hereten mai automàticament. Per utilitzar un d'aquests
privilegis que s'hagi assignat a un rol diferent del que ha utilitzat l'usuari
per connectar-se hem d'utilitzar sempre `SET ROLE`.
====

Podem utilitzar `\du` per veure els privilegis globals de tots els rols i quins
rols pertanyen a altres rols.

== Gestió dels privilegis

=== Propietari

Tots els objectes de PostgreSQL (bases de dades, taules, columnes, programes,
etc.) tenen un rol propietari, habitualment qui els ha creat.

El propietari d'un objecte té permisos complets sobre aquest objecte, i és
l'únic que pot eliminar-lo o modificar-lo.

A més, el propietari pot atorgar privilegis a altres rols per tal que puguin
utilitzar els seus objectes.

Els administradors (_superusers_) i el propietari d'un objecte pot
traspassar-ne la propietat a un altre rol utilitzant l'ordre `ALTER`. El
propietari només ho pot fer si també pertany al rol destinatari.

.Canvi de propietari d'una taula
====
Per exemple, podem utilitzar la següent ordre per atorgar la propietat de
la taula `nomines` al rol `personal`:

----
=> ALTER TABLE nomines OWNER TO personal;
----
====

=== Privilegis

Alguns dels possibles privilegis que es poden atorgar són els següents:

- `SELECT`, `INSERT`, `UPDATE`, `DELETE`, `TRUNCATE`: permeten realitzar les
operacions corresponents sobre l'objecte (taula, columna o vista), així com
altres operacions que necessiten d'aquests permisos per poder-se realitzar (
per exemple, si per fer un `UPDATE` necessitem referir-nos a valors que ja
existeixen, haurem de tenir també `SELECT`).

- `REFERENCES`: permet la creació de claus foranes. Necessitem tenir aquest
permís sobre les dues taules afectades.

- `TRIGGER`: permet la creació de disparadors sobre una taula.

- `CREATE`: depenent d'on s'hagi atorgat el privilegi, permet la creació
d'esquemes, taules, índexs...

- `CONNECT`: permet a un usuari connectar-se a una base de dades.

- `TEMP`: permet la creació de taules temporals en una base de dades.

- `EXECUTE`: permet l'execució d'una determinada funció.

- `USAGE`: depèn de l'objecte on s'assigna. Pot permetre l'ús d'un determinat
llenguatge de programació per crear nous procediments, o la consulta dels
objectes que hi ha creats en un determinat esquema.

- `ALL PRIVILEGES`: atorga tots els privilegis sobre un objecte.

=== Assignació de privilegis

Per assignar privilegis a rols que no siguin el propietari d'un objecte
s'utilitza la instrucció `GRANT`.

==== Privilegis per taules

----
GRANT { { SELECT | INSERT | UPDATE | DELETE | TRUNCATE | REFERENCES | TRIGGER }
    [, ...] | ALL [ PRIVILEGES ] }
    ON { [ TABLE ] table_name [, ...]
         | ALL TABLES IN SCHEMA schema_name [, ...] }
    TO role_specification [, ...] [ WITH GRANT OPTION ]
----

.Assignació de privilegis a taules
====

- Dóna permís de lectura a totes les taules de `pagila` a l'usuari `joan`:
+
----
=> GRANT SELECT ON ALL TABLES IN SCHEMA pagila TO joan;
----

- Dóna permís de modificació sobre les taules `film`, `film_actor` i `actor`
a l'usuari `joan`.
+
----
=> GRANT INSERT, UPDATE, DELETE ON film, film_actor, actor TO joan;
----
====

==== Privilegis per columnes

----
GRANT { { SELECT | INSERT | UPDATE | REFERENCES } ( column_name [, ...] )
    [, ...] | ALL [ PRIVILEGES ] ( column_name [, ...] ) }
    ON [ TABLE ] table_name [, ...]
    TO role_specification [, ...] [ WITH GRANT OPTION ]
----

==== Privilegis per seqüències

----
GRANT { { USAGE | SELECT | UPDATE }
    [, ...] | ALL [ PRIVILEGES ] }
    ON { SEQUENCE sequence_name [, ...]
         | ALL SEQUENCES IN SCHEMA schema_name [, ...] }
    TO role_specification [, ...] [ WITH GRANT OPTION ]
----

==== Privilegis per bases de dades

----
GRANT { { CREATE | CONNECT | TEMPORARY | TEMP } [, ...] | ALL [ PRIVILEGES ] }
    ON DATABASE database_name [, ...]
    TO role_specification [, ...] [ WITH GRANT OPTION ]
----

==== Privilegis per funcions

----
GRANT { EXECUTE | ALL [ PRIVILEGES ] }
    ON { FUNCTION function_name ( [ [ argmode ] [ arg_name ] arg_type [, ...] ] ) [, ...]
         | ALL FUNCTIONS IN SCHEMA schema_name [, ...] }
    TO role_specification [, ...] [ WITH GRANT OPTION ]
----

==== Privilegis per esquemes

----
GRANT { { CREATE | USAGE } [, ...] | ALL [ PRIVILEGES ] }
    ON SCHEMA schema_name [, ...]
    TO role_specification [, ...] [ WITH GRANT OPTION ]
----

=== Revocació de privilegis

----
=> REVOKE CREATE ON myschema FROM joan;
----

=== Privilegis per defecte i rol `PUBLIC`

La paraula clau `PUBLIC` s'utilitza per atorgar alguns privilegis a tots els
rols, fins i tot a aquells que es crearan posteriorment. `PUBLIC` es pot pensar
com un grup implícit que sempre inclou a tots els rols.

Com que qualsevol rol té tots els privilegis que se li han atorgat directament,
més els privilegis dels rols dels quals és membre, també tindrà qualsevol
privilegi atorgat a PUBLIC.

A més, el PostgresSQL atorga alguns permisos per defecte a PUBLIC:

- Per bases de dades, CONNECT i CREATE TEMP TABLE.
- Per funcions, EXECUTE.
- Per llenguatges de programació, USAGE.

Els privilegis per defecte es poden retirar utilitzant REVOKE, o es pot
modificar quins són els privilegis per defecte que s'atorguen utilitzant
ALTER DEFAULT PRIVILEGES.

Finalment, quan es crea una base de dades, automàticament es crea l'esquema
`public` i s'atorguen permisos d'ús i creació en aquest esquema a `PUBLIC`.

=== Mostrar els privilegis

----
=> GRANT SELECT ON mytable TO PUBLIC;
=> GRANT SELECT, UPDATE, INSERT ON mytable TO admin;
=> GRANT SELECT (col1), UPDATE (col1) ON mytable TO miriam_rw;
----

----
=> \dp mytable
                              Access privileges
 Schema |  Name   | Type  |   Access privileges   | Column access privileges
--------+---------+-------+-----------------------+--------------------------
 public | mytable | table | miriam=arwdDxt/miriam | col1:
                          : =r/miriam             :   miriam_rw=rw/miriam
                          : admin=arw/miriam
(1 row)
----

----
rolename=xxxx -- privileges granted to a role
        =xxxx -- privileges granted to PUBLIC

            r -- SELECT ("read")
            w -- UPDATE ("write")
            a -- INSERT ("append")
            d -- DELETE
            D -- TRUNCATE
            x -- REFERENCES
            t -- TRIGGER
            X -- EXECUTE
            U -- USAGE
            C -- CREATE
            c -- CONNECT
            T -- TEMPORARY
      arwdDxt -- ALL PRIVILEGES (for tables, varies for other objects)
            * -- grant option for preceding privilege

        /yyyy -- role that granted this privilege
----

=== Privilegis del propietari

Els privilegis especials del propietari d'un objecte, com el dret a esborrar-lo
(DROP) o el dret a donar-hi o treure-hi privilegis (GRANT i REVOKE), són
implícits pels fer de ser el propietari i no es poden atorgar o revocar. Tot i
així, el propietari pot optar per revocar els seus propis privilegis, per
exemple, fer que una taula sigui de només lectura per a sí mateix.

Habitualment, només el propietari d'un objecte pot atorgar o revocar privilegis
sobre un objecte. Tanmateix, és possible atorgar un privilegi amb "opció
d'atorgar" (`WITH GRANT OPTION`), cosa que dóna al beneficiari el dret
d'atorgar al seu torn el privilegi a altres.

Si l'opció d'atorgar és revocada posteriorment, tots aquells que han rebut
privilegis a través d'aquest mecanisme els perdran.
