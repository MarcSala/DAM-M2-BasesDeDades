= Activitats extraescolars
:doctype: article
:encoding: utf-8
:lang: ca
:toc: left
:toclevels: 3
:numbered:

<<<

== Plantejament

Volem crear un model entitat-relació que modelitzi les activitats
extraescolars que ofereixen les escoles del Bages.

En concret, tenim els següents requeriments:

- Cada escola té un codi que la identifica. De cada escola volem saber-ne el
nom i la població on està situada. A més, de cada escola volem saber les
activitats extraescolars que organitza.

- Cada escola pot oferir diverses activitats extraescolars. Només
guardarem a la base de dades escoles que ofereixin com a mínim una activitat
extraescolar. De la mateixa manera, només guardarem a la base de dades
activitats per a les quals hi hagi com a mínim una escola que l'ofereixi.

- De cada activitat que oferta una escola volem saber, a més del seu nom, la
quantitat de places que ofereix. També volem enregistrar la puntuació que li
van atorgar el curs anterior els nens i nenes que hi van participar. Aquesta
puntuació és un nombre enter entre 0 i 10.

- Voldrem guardar un registre dels nens i nenes que van a cadascuna de les
escoles. No necessitem saber quines activitats està fent cadascun d'ells,
només a quina escola van.

- Volem conèixer el seu codi (que se'ls assigna per poder-los
identificar fàcilment), el seu nom i cognoms, la població on viuen i un telèfon
de contacte.

- Hi poden haver activitats oferides a les quals no s'hagi apuntat ningú.

- De les poblacions on hi ha escoles que realitzen activitats o on hi viuen
alguns dels nens o nenes en volem saber el seu nom i la quantitat d'habitants
que té.
