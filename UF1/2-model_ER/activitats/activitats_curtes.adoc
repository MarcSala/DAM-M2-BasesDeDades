= Activitats curtes
:doctype: article
:encoding: utf-8
:lang: ca
:toc: left
:toclevels: 3
:numbered:

<<<

== Registre de matrícula

L'institut vol crear una petita base de dades per saber a quines unitats
formatives està matriculat cada alumne.

Dels alumnes en guardaran el seu nom i cognoms, la seva data de naixement, i
el seu RALC (Registre d'Alumnat, un número únic per cada alumne que assigna
la Generalitat).

Per altra banda, es guardaran els cicles formatius que s'imparteixen a
l'institut. De cada cicle formatiu es guardarà el seu nom, la seva abreviatura
(per exemple, DAM), si és de grau superior o grau mitjà, i un codi únic que
assigna la Generalitat (per exemple, ICB0 és DAM i ICB2 és DAMvi).

Per cada cicle es guardaran els seus mòduls (número del mòdul, codi únic del
mòdul, nom del mòdul i hores totals), i per cada mòdul, les unitats formatives
que els componen (número i nom de la UF, quantitat d'hores, i codi únic).

Cal tenir en compte que mòduls i unitats formatives equivalents de cicles
diferents (per exemple, bases de dades de DAM i bases de dades de DAMvi) es
consideren mòduls diferents i tenen un codi diferent.

Finalment, ha de quedar registrat a quines UF està matriculat cadascun dels
alumnes.

Font: link:http://educacio.gencat.cat/portal/page/portal/Educacio/PCentrePrivat/PCPIntercanviDades/PCPSistemaIntercanvi[Taules de codis del catàleg dels cicles formatius d'FP]

== Agenda cultural

L'Ajuntament vol oferir un servei de consulta de l'agenda cultura de la ciutat
a través de la seva pàgina web. El sistema anirà recolzat per una base de dades
amb informació d'aquests esdeveniments.

De cada esdeveniment programat se'n guardarà un codi únic, un títol, una petita
descripció, el lloc on es fa, l'hora d'inici, la durada prevista, i el preu.

Dels diversos llocs on es fan esdeveniments, a l'Ajuntament li interessa
saber-ne el seu nom, adreça (una única cadena de text), i el tipus d'espai
(si és un lloc públic, el lloc d'una entitat sense ànim de lucre, o un negoci
privat). Els diversos tipus d'espais possibles els guardarem a la base de dades
per evitar errors en la introducció de dades.

Cada activitat es pot realitzar una o més vegades, així que hem de poder guardar
tants horaris com calgui per una mateixa activitat. A més, una mateixa activitat
es pot repetir a llocs diferents, i cada lloc pot posar un preu diferent.

Finalment, un cop passada l'activitat, l'Ajuntament guardarà la quantitat
aproximada d'assistents que hi ha hagut a cadascuna de les representacions.

== Control de visites

Al Centre Oficial d'Entitat-Relació volen implantar un sistema de control de
visites.

Per a cada visitant volen guardar-ne el seu nom complet, el seu DNI, i un
número de telèfon, i volen registrar el moment (data i hora) de la seva entrada
i de la seva sortida.

Cal tenir en compte que un mateix visitant pot fer diverses visites al Centre.

== Afició per l'aviació

Uns aficionats a l'aviació volen mantenir una base de dades amb un registre
de tots els vols programats que hi ha al país.

Per cada vol volen guardar la seva data i hora de sortida i arribada previstes,
el nom de la companyia que nolieja l'avió, el nom del model d'avió que està
previst que realitzi el vol, i els aeroports i terminals de sortida i de destí.
Cada vol té un codi que identifica el vol dins de la planificació setmanal de
tots els vols.

De cada aeroport volen saber-ne el seu codi identificador, el seu nom, la ciutat
on es troba, i la quantitat de terminals que té. De cada terminal en guardaran
el nom i les coordenades geogràfiques on es troba.

== La llibreria

En una petita llibreria volen començar a gestionar el seu estoc amb una base
de dades.

De cada llibre que vénen n'han de guardar el seu títol, el seu ISBN,
el seu autor o autors, el seu preu, l'editorial que el publica, la distribuïdora
que els l'ha distribuït, i si el llibre està en dipòsit (el poden tornar si no
el vénen) o en ferm (no el poden retornar). També guardaran la quantitat
d'exemplars que tenen en estoc de cada llibre.

Cal tenir en compte que d'un mateix llibre hi poden haver-hi exemplars que els
hagin proporcionat diverses distribuïdores, i/o que alguns estiguin en dipòsit
i d'altres en ferm.

== El fòrum

Volem implementar un fòrum web amb funcionalitats mínimes. El fòrum utilitzarà
una base de dades per guardar els seus usuaris i els missatges que es
publiquin.

De cada usuari en guardarem el seu nom d'usuari, el seu correu electrònic,
i una contrasenya xifrada. Ni el nom d'usuari ni el correu electrònic poden
repetir-se. Alguns dels usuaris seran moderadors i podran modificar o
eliminar els missatges del fòrum.

Els usuaris podran escriure missatges al fòrum. Els missatges tenen un
assumpte, i un cos de text. Cal recordar qui és l'autor de cada missatge i quin
dia i a quina hora s'ha publicat. El sistema generarà un identificador únic per
a cada missatge.

A més, els usuaris podran respondre als missatges d'altres usuaris. Les
respostes són idèntiques als missatges originals, amb l'única diferència que
fan referència a un missatge existent.

== Inventari de satèl·lits

Una associació de científics porta anys recopilant informació sobre els
satèl·lits que hi ha en òrbita de la Terra en un full de càlcul. Ara volen
convertir aquest full en una base de dades, per tal de facilitar-ne les
consultes.

De cada satèl·lit guardarem la següent informació:

- El seu nom o noms. Un satèl·lit pot tenir diversos noms, però sempre en
tindran un que el considerarem el seu nom estàndard. Els noms de cada satèl·lit
en principi no es repeteixen, però no hi ha cap garantia que això no pugui
passar en un futur.
- El país que el té registrat. No tots els satèl·lits estan registrats, si ho
estan, ho estan en un país.
- El país o països que l'operen. Pot ser un país, diversos països, o es pot
considerar _multinacional_ si l'opera un conglomerat de països.
- El seu ús (civil, comercial, militar...). Un satèl·lit pot tenir un o més
usos. Per evitar errades, guardarem una llista amb tots els possibles usos
d'un satèl·lit.
- Classe d'òrbita, d'una llista concreta, per exemple, _òrbita terrestre baixa_.
- La data de llançament.
- El número COSPAR. Es tracta d'un codi únic assignat a cada objecte espacial
pel Comitè de Recerca Espacial. Per exemple, _2017-036L_.

Font: link:https://www.ucsusa.org/nuclear-weapons/space-weapons/satellite-database[UCS Satellite Database]
