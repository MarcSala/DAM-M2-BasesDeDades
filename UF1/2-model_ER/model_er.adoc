= Disseny conceptual: el model Entitat-Relació
:doctype: article
:encoding: utf-8
:lang: ca
:toc: left
:toclevels: 3
:numbered:
:ascii-ids:

<<<

== Introducció

El _model de dades entitat relació (ER)_ existeix des del 1976. És adequat per
modelar dades per a ser utilitzades en una base de dades perquè
és força abstracta i és fàcil de discutir i explicar. Els models ER són
directament traduïbles al model relacional que utilitzen els SGBD relacionals.
Els models ER, també anomenats esquemes ER, es representen a través de diagrames
ER.

El modelatge ER es basa en tres conceptes:

* _Entitats_: objectes del món real, dels quals ens interessa modelitzar algunes
propietats, i que són distingibles d'altres objectes.
* _Relacions_: associacions i interaccions entre entitats.
* _Atributs_: són les propietats que ens interessen de les entitats i d'algunes
relacions.

Aquí hi ha un exemple de com entitats i relacions es poden combinar en un model
de dades ER: el professor Foo (entitat) imparteix (relació) el curs de sistemes
de bases de dades (entitat).

Durant la resta d'aquest apartat, utilitzarem una base de dades d'exemple
anomenada la base de dades `COMPANY` per il·lustrar els conceptes del model ER.
Aquesta base de dades conté informació sobre empleats, departaments i
projectes. Alguns punts importants a notar són:

* Hi ha diversos departaments a la companyia. Cada departament té un
identificador únic, un nom, localització de l'oficina i un empleat
concret que gestiona el departament.
* Un departament controla un nombre de projectes, cadascun dels quals té un
nom únic, un número únic i un pressupost.
* Cada empleat té un nom, número identificatiu, DNI, adreça, sou i data de
naixement. A cada empleat se li assigna un sol departament, però pot unir-se a
diversos projectes. Necessitem guardar la data en què l'empleat s'uneix a cada
projecte. També necessitem saber el supervisor directe de cada empleat.

== Entitats

Una _entitat_ és un objecte en el món real amb una existència independent que
es pot diferenciar d'altres objectes. Una entitat pot ésser:

* Un objecte amb existència física (per exemple, un conferenciant, un estudiant,
un cotxe).
* Un objecte amb existència conceptual (per exemple, un curs, una feina, un
càrrec).

El terme entitat s'utilitza de forma una mica ambigua: per una banda, es
refereix a un objecte individual, una instància concreta, com per exemple un
empleat en concret. Per altra banda, també es pot referir al conjunt de totes
les entitats del mateix tipus; per exemple, al conjunt de tots els empleats.

En un diagrama entitat-relació es representen realment els _tipus d'entitat_
que defineixen una col·lecció d'entitats similars. Un tipus d'entitat es
representa amb el nom en majúscules dins d'una caixa.

[IMPORTANT]
====
Per evidenciar que ens estem referint a totes les entitats d'un cert tipus,
posarem sempre el nom en plural.
====

Per exemple, a la figura següent es mostra el tipus d'entitat `EMPLOYEES`:

.ERD del tipus d'entitat EMPLOYEES.
image::images/entity_set.png["ERD del tipus d'entitat EMPLOYEES",scaledwidth="50%"]

== Atributs

Cada tipus d'entitat es descriu per un conjunt d'atributs, que són les dades que
volem emmagatzemar sobre cada entitat d'aquell tipus. Per exemple, d'un
empleat ens interessa saber el seu número identificador, el seu DNI, el nom i
cognoms, la data de naixement, i el seu sou.

En el diagrama entitat relació de la següent figura, cada atribut es representa
per un oval amb un nom a dins:

.Com es mostren els atributs en un ERD.
image::images/attributes.png["Com es mostren els atributs en un ERD",scaledwidth="40%"]

Per tal d'ocupar menys espai, els atributs es poden representar també unint-los
a l'entitat amb un guió, o es poden escriure directament a banda del diagrama
si n'hi ha molts.

Als atributs se'ls assigna un tipus de dada i un domini del qual poden prendre
els seus valors. El _domini_ és el conjunt de tots els valors vàlids per un
atribut. Tanmateix, la informació sobre el domini d'un atribut no es mostra en
el ERD.

Per exemple, si l'atribut guardarà el resultat de les tirades d'un dau, el seu
domini serà el conjunt {1, 2, 3, 4, 5, 6} i el seu tipus de dades serà enter.

Els atributs poden ésser de diversos tipus:

=== Atributs univaluats i multivaluats

Els _atributs univaluats_ són aquells que reben un sol valor per a cada
ocurrència d'una entitat. A la base de dades COMPANY, un exemple
d'això podria ser: `Name = {John}, Age = {23}`

[IMPORTANT]
====
Tots els atributs han d'ésser univaluats.
====

Els _atributs multivaluats_ són atributs que tenen un conjunt de valors per a
cada entitat.

Un exemple d'atributs multivaluat de la base de dades COMPANY,
com es veu a la figura següent, són les titulacions acadèmiques d'un empleat:

.Exemple d'atribut multivaluat.
image::images/multivalued_attribute.png["Exemple d'atribut multivaluat",scaledwidth="50%"]

Com hem dit, tots els atributs han de ser univaluats, així que caldria
modificar aquest diagrama per solucionar aquest problema.

Per fer-ho, podem considerar que les titulacions són realment una entitat, i
establir una relació entre els empleats i les titulacions que tenen. Ho veurem
en detall més endavant.

=== Atributs simples i compostos

Els _atributs simples_ són aquells que sorgeixen de dominis de valors atòmics,
és a dir, que no es poden dividir en valors més simples. A la base de dades
COMPANY, un exemple d'això podria ser: `Name = {John}, Age = {23}`

Els _atributs compostos_ són aquells els valors dels quals sí que es poden
dividir en atributs més senzills. Utilitzant la nostra base de dades d'exemple,
com es mostra a la següent figura, `Address` pot consistir en un nombre, un
carrer, un codi postal i una localitat.

.Un exemple d'atributs compostos.
image::images/simple_and_composite_attributes.png["Un exemple d'atributs compostos",scaledwidth="75%"]

En aquests casos cal plantejar-se la conveniència de dividir l'atribut
compost en les seves parts simples, cosa que pot reduir el nivell de
redundància de la base de dades i pot facilitar el tractament de les dades.

[IMPORTANT]
====
Un atribut mai es pot descompondre en diversos atributs.
====

.Un atribut no pot tenir atributs.
image::images/attribute_with_attributes.png["Un atribut no pot tenir atributs",scaledwidth="75%"]

Per resoldre aquesta situació caldria o bé eliminar l'atribut `Address` i posar
tots els seus atributs simples a `EMPLOYEES`, o bé convertir les adreces en
una nova entitat `ADDRESSES`, amb els seus atributs. Veurem en detall aquestes
possibilitats més endavant.

=== Atributs derivats

Els _atributs derivats_ són atributs que contenen valors que s'han calculat
a partir d'altres atributs. Un exemple d'això es pot veure a la següent
figura: l'edat (`Age`) es pot derivar a partir de l'atribut data de naixement
(`BirthDate`). En aquest cas, la data de naixement s'anomena _atribut
emmagatzemat_, perquè es guarda físicament a la base de dades.

.Exemple d'atribut derivat.
image::images/derived_attribute.png["Exemple d'atribut derivat",scaledwidth="60%"]

Aquí cal considerar si és convenient o no guardar l'atribut derivat. Per
exemple, si es modifica la data de naixement d'un empleat, i no som curosos,
podria produir-se una incoherència entre l'edat i la data de naixement.

En general, només guardarem atributs derivats quan sigui una dada que ens
interessa especialment i el càlcul de la qual no és immediat. Per exemple,
podríem guardar el total d'una factura, per no haver de mirar tots els ítems
que s'han comprat cada vegada que necessitem aquesta dada.

== Claus

Una _clau_ és un atribut o conjunt d'atributs d'un tipus d'entitat els valors
dels quals es poden utilitzar per identificar de forma única una entitat
individual entre totes les entitats del mateix tipus.

Per exemple, el DNI permet identificar de forma única un empleat.

=== Claus candidates

Pot passar que una entitat tingui diverses claus. Cadascuna d'aquestes claus
s'anomena _clau candidata_.

Del nostre exemple de la base de dades COMPANY, si l'entitat és `Projects =
(Code, Name, StartingDate, Description, Budget)`, tenim aquestes possibles claus
candidates:

* `Code`: a cada projecte se li assigna un codi únic.
* `Name`: pot ser una clau candidata si estem segurs que dos projectes mai
tindran el mateix nom.
* `Name, StartingDate`: si dos projectes poden tenir el mateix nom, és raonable
suposar que dos projectes mai tindran el mateix nom i començaran el mateix dia.
* `Name, Description`: també és raonable suposar que dos projectes no tindran
el mateix nom i la mateixa descripció.

=== Claus simples i compostes

Una clau s'anomena _clau simple_ si consisteix en només un atribut, o _clau
composta_ si es compon de dos o més atributs.

A l'exemple anterior, `Code` i `Name` són claus simples, i `Name, StartingDate`
i `Name, Description` són compostes.

Una clau composta ha de ser minimal, cosa que significa que si traiem
qualsevol dels seus atributs de la clau, els atributs resultants deixen
d'identificar a l'entitat de forma única.

Així, si és cert que el nom d'un projecte l'identifica de forma única, les
claus `Name, StartingDate` i `Name, Description` serien incorrectes, perquè
són minimals. En canvi, si no podem assegurar que el nom és suficient, aquestes
dues claus són vàlides.

=== Clau primària

De totes les claus candidates disponibles, se'n tria una que serà la que
habitualment utilitzem per identificar les entitats d'aquell tipus. Aquesta
clau s'anomena _clau primària_.

La clau primària s'indica en el model ER subratllant-ne l'atribut o atributs que
la formen.

Totes les entitats del tipus han de tenir algun valor vàlid per a la seva clau
primària. Per exemple, si a l'exemple anterior triem el codi com a clau
primària dels projectes, no pot ser que algun projecte no tingui cap codi
assignat.

[IMPORTANT]
====
Tots els tipus d'entitat han de tenir una clau primària.
====

En el següent exemple, el `Code` és la clau primària:

====
*Projects*([underline]#Code#, Name, StartingDate, Description, Budget)
====

.Exemple de clau primària.
image::images/primary_key.png["Exemple de clau primària",scaledwidth="40%"]

Alguns criteris per triar una clau primària:

- Hem d'estar absolutament segurs que es tracta efectivament d'una clau, no
només amb les dades que tenim actualment, si no també per qualsevol dada nova
que puguem rebre en un futur.
+
Per exemple, abans hem afirmat que el DNI era una clau candidata pels empleats.
Estem absolutament segurs que tots els empleats tindran DNI? Si un empleat ve
d'un altre país de la Unió Europea, podria estar treballant aquí un temps sense
un DNI espanyol. Aleshores, el DNI ja no seria una bona clau primària pels
nostres empleats.

- Ha de tenir el mínim nombre d'atributs possibles. Si podem, triarem una clau
simple, si no, millor que siguin dos atributs que no pas tres.
+
Les claus primàries són de les poques dades que sí que apareixen duplicades en
una base de dades. També, les claus primàries s'indexen per tal de poder-se
cercar ràpidament. Per tant, la nostra base de dades tindrà un disseny més
senzill i serà més eficient si les claus primàries són simples.

- És millor que els tipus de dades dels atributs de les claus primàries siguin
senzills i ocupin poc espai. Per exemple, millor un nombre enter (que sempre
ocupa el mateix nombre de bits), que no una cadena de text de longitud variable.
Aquest és el motiu pel qual a l'exemple dels projectes preferíem agafar el codi,
que podem pensar com un text curt que sempre té la mateixa longitud, que no
pas el nom, que pot tenir una longitud molt variable.

=== Clau alternativa

Les _claus alternatives_ són totes les claus candidates que no s'han
seleccionat com a la clau primària.

=== Claus sintètiques

Actualment, a l'hora d'implementar una base de dades s'utilitzen claus
sintètiques en la major part dels casos. Una _clau sintètica_ és una clau
inventada, que assigna un número arbitrari a cada entitat amb l'únic objectiu
d'identificar-les, però que no representa cap atribut real de l'entitat.

Les claus sintètiques tenen l'avantatge que sempre són nombres enters i que
sempre acostumen a tenir el mateix nom (`Id`). Això fa que algunes eines que
automatitzen aspectes de la programació contra bases de dades, com els ORM,
ho tinguin més fàcil. A més, totes les claus són simples, ocupen poc espai, i
són fàcilment indexables.

Una clau sintètica presenta també alguns inconvenients. Per una banda, tenim un
atribut de més que no aporta cap informació real addicional. Per altra banda, i
això és més delicat, podria passar que introduíssim dues vegades la mateixa
entitat a una BD sense adonar-nos-es, perquè el segon cop se li assignaria un
_Id_ diferent.

A nivell de disseny ER seguirem els següents criteris a l'hora d'utilitzar
claus sintètiques:

Sempre triarem preferentment una clau primària _natural_, és a dir, formada
per atributs existents al tipus d'entitat. Reservarem l'ús de claus sintètiques
només als casos en què trobar una clau primària natural sigui impossible, o
acabi sent molt complexa (per exemple, formada per tots els atributs de
l'entitat).

Encara que més endavant, quan implementem la BD, decidim utilitzar claus
sintètiques, haver pensat el disseny d'aquesta manera ens ajudarà a garantir que
el disseny està ben fet (hem triat bé les entitats), i ens permetrà definir
claus alternatives que evitaran problemes com la introducció d'entitats
duplicades.

== Relacions

Les _relacions_ són associacions entre tipus d'entitats. S'utilitzen per
connectar informació relacionada entre tipus d'entitats.

Les relacions es representen amb un rombe als diagrames ER. El rombe es
connecta amb les entitats relacionades mitjançant línies contínues.

=== Grau de les relacions

Una relació associa com a mínim dues entitats, però en pot associar més. La
quantitat d'entitats associades a través d'una relació s'anomena _grau_ de la
relació.

Les relacions que involucren dues entitats s'anomenen _relacions binàries_ i
són les més habituals. Per exemple, la relació d'un empleat amb el departament
al qual pertany, o d'un projecte amb els empleats que hi participen, són
relacions binàries.

Una _relació ternària_ és un tipus de relació en què participen tres tipus
d'entitats. En general, una relació _n-ària_ involucra _n_ entitats.

=== Relacions binàries

Les relacions més habituals associen dos tipus d'entitats i s'anomenen
_relacions binàries_.

En el següent exemple es modelitza la matrícula dels estudiants a les diverses
assignatures que hi ha:

.Exemple de relació
image::images/relationship.png["Exemple de relació",scaledwidth="40%"]

El diagrama ens indica que _alguns_ alumnes estan matriculats d'_algunes_
assignatures. És a dir, una relació no implica que totes les entitats d'un cert
tipus hagin d'estar associades a entitats de l'altre tipus, sinó que n'hi ha
prou amb què algunes ho estiguin.

El que és important en les relacions és que dues entitats concretes o estan
relacionades, o no estan relacionades, però no ho poden estar més d'una vegada.
Aplicat això al nostre exemple, un alumne pot estar matriculat d'una assignatura
o pot no estar-hi, però no té sentit que estigui matriculat de la mateixa
assignatura diversos cops.

Si detectem que això és una possibilitat, caldrà modificar el diagrama. Per
exemple, si considerem totes les matricules dels alumnes al llarg del temps,
sí que es pot donar que un alumne s'hagi matriculat més d'un cop a la mateixa
assignatura. Per fer que el diagrama sigui correcte en aquest cas, caldrà
afegir el curs com una tercera entitat a la relació, obtenint així una
relació ternària. Analitzarem en detall aquest cas més endavant.

A banda d'això, els nombres que apareixen a la relació són la cardinalitat, de
la qual es parla una mica més endavant. I l'atribut _RALC_ fa referència al
codi únic que s'utilitza per identificar a l'alumnat a Catalunya (l'alumnat
més jove pot no tenir DNI i, per tant, el DNI no seria una bona clau primària).

=== Atributs a les relacions

Algunes relacions poden tenir també atributs que reflecteixen propietats de
la relació. Aquests atributs són com els de les entitats: tenen un tipus de
dades, un domini, i els seus valors han de ser atòmics i univaluats.

Els atributs es representen als diagrames ER unint el seu nom en minúscules
al rombe de la seva relació mitjançant un guió.

Un exemple típic d'atribut a una relació són les notes que obtenen els
estudiants a les assignatures que estan cursant.

Fixem-nos que l'atribut nota no pot ser d'estudiant, perquè un estudiant té
diverses notes, una per assignatura. Tampoc no pot pertànyer a assignatura,
perquè la mateixa assignatura és cursada per molts estudiants, i hem de guardar
una nota per a cadascun d'ells. Així que l'única solució possible és posar
l'atribut nota com un atribut de la relació entre estudiant i assignatura.

.Exemple d'atribut en una relació.
image::images/relationship_attribute.png["Exemple d'atribut en una relació",scaledwidth="40%"]

=== Connectivitat de les relacions binàries

La _connectivitat_ d'una relació és el tipus de correspondència entre les
entitats que participen de la relació.

En el cas de relacions binàries, la connectivitat expressa la quantitat
d'entitats concretes de cada tipus que es relaciona amb una entitat de l'altre
tipus.

Hi ha tres tipus de connectivitat per a les relació binàries: un a un, un a
molts, o molts a molts.

==== Relacions un a un (1:1)

Una relació un a un (1:1) és una relació d'una entitat cap a només una altra
entitat, i viceversa.

És el tipus de relació més senzilla que hi ha, però és poc habitual en
el disseny de qualsevol base de dades relacional. De fet, podria indicar que
dues entitats són realment la mateixa entitat.

Per exemple, un cotxe porta un motor, i un motor només pot estar en un cotxe.

.Exemple de relació d'un a un.
image::images/one_to_one_relationship.png["Exemple de relació d'un a un",scaledwidth="40%"]

==== Relacions un a molts (1:N)

Les relacions d'un a molts (1:N) són les més habituals en el disseny de
qualsevol base de dades relacional. Una entitat d'un tipus pot estar
relacionada amb moltes entitats de l'altre tipus, però una entitat de l'altre
tipus només està relacionada amb una entitat del primer tipus.

Per exemple, un departament té molts empleats, i un empleat pertany només a un
departament.

La següent figura mostra la relació d'un d'aquests empleats cap al departament.

.Exemple de relació d'un a molts.
image::images/one_to_many_relationship.png["Exemple de relació d'un a molts",scaledwidth="75%"]

Les relacions que identifiquen a les entitats dèbils sobre són d'aquest tipus,
i l'entitat dèbil sempre ocupa el costat N.

==== Relacions molts a molts

Una relació de molts a molts es dóna quan una entitat d'un tipus pot estar
relacionada amb diverses entitats de l'altre tipus i a la inversa. Per exemple,
un alumne pot cursar diverses matèries, i molts alumnes poden estar cursant la
mateixa matèria.

La figura següent mostra un altre exemple de relació N:M en què un
empleat té diverses dates d'inici per a diversos projectes. En aquest exemple,
la data d'incorporació de l'empleat al projecte és un atribut de la relació.

.Exemple en què un empleat té diverses dates d'inici per diferents projectes.
image::images/many_to_many_relationships.png[Exemple en què un empleat té diverses dates d'inici per diferents projectes,scaledwidth="75%"]

=== Connectivitats mínimes

A més de decidir la connectivitat general de les relacions binàries (1 a 1, 1
a molts, o molts a molts), també ens interessarà determinar la connectivitat
mínima.

Si els tipus d'entitat _A_ i _B_ estan relacionades, la connectivitat mínima és
el mínim número d'entitats del tipus _B_ que obligatòriament estan relacionades
amb cadascuna de les entitats del tipus _A_.

De la mateixa manera, també determinarem la connectivitat en l'altre sentit,
de _B_ cap a _A_.

Ho veurem clar amb alguns exemples:

* Cada departament ha de tenir un empleat que faci de cap. No hi ha cap
departament que no tingui un cap. Així, la connectivitat mínima entre
departament i empleat és 1.

* En canvi, un empleat pot ser o no cap d'algun departament. És a dir, hi ha
empleats que no són caps de cap departament. Per tant, la connectivitat
mínima entre empleat i departament és 0.

La connectivitat mínima es representa juntament amb la connectivitat general,
escrivint primer la mínima, seguida de .. (punt punt), i de la connectivitat
general. La connectivitat mínima no s'escriu si coincideix amb la general:

.Exemple de connectivitat mínima.
image::images/min_connectivity.png["Exemple de connectivitat mínima",scaledwidth="40%"]

La relació que es mostra a la figura indica que per a cada departament,
sense excepció, ha d'existir un empleat que sigui el seu cap, i cada empleat
pot ser cap de zero o més departaments.

=== Relacions recursives

Una _relació recursiva_ és una relació que associa diverses ocurrències del
mateix conjunt d'entitats. Veiem un exemple a la següent figura:

.Exemple de relació unària.
image::images/unary_relationships.png["Exemple de relació unària",scaledwidth="60%"]

=== Relacions ternàries

Les relacions ternàries són aquelles en què hi ha tres entitats participants.

Considerem la següent situació: en un institut s'imparteixen diverses matèries.
Volem guardar en una base de dades quines matèries s'imparteixen a cadascuna de
les aules, a cada hora de l'horari. Naturalment, en una aula no es pot impartir
més d'una matèria a la vegada.

En un primer moment, podríem pensar que l'horari és un atribut de la relació
entre _Assignatures_ i _Aules_ (es tractaria realment de dos atributs, perquè
inclou un dia, per exemple dilluns, i una hora, per exemple les 9):

image::images/ternary_relationship_as_one_binary.png["Exemple de relació ternària com una binària",scaledwidth="65%"]

Però una mateixa matèria es pot impartir a una mateixa aula en més d'un horari.
Per exemple, suposem que es fan matemàtiques a l'aula A1 el dilluns a les 9h i
el dimecres a les 10h.

Aleshores, tindríem només dues opcions: o els atributs de l'horari són
multivaluats (perquè per a la parella A1-Matemàtiques han de contenir més d'una
dada), o bé les entitats A1 i Matemàtiques haurien d'estar relacionades
més d'una vegada. *Cap de les dues opcions és vàlida en el model ER.*

Així, sembla que el millor camí és interpretar l'horari com una entitat, i fer
que la relació sigui ternària:

.Exemple de relació ternària
image::images/ternary_relationship.png["Exemple de relació ternària",scaledwidth="65%"]

Quan parlem d'una relació ternària implica que parlem de grups de tres entitats,
una de cadascun dels tres tipus, que estan relacionades entre elles.

No té sentit que en una relació ternària hi hagi dues entitats relacionades,
sempre han de ser conjunts de tres.

Com en les relacions binàries, tampoc té sentit que el mateix conjunt de tres
entitats estiguin relacionades dues vegades (encara que sí que es poden repetir
dues de les tres entitats).

A l'exemple anterior, podem tenir dades com:

[options="headers"]
|===
|Aules |Matèries     |Horaris
|A1    |Matemàtiques |Dilluns a les 9h
|A1    |Matemàtiques |Dimecres a les 10h
|A2    |Matemàtiques |Dijous a les 9h
|A1    |Història     |Dilluns a les 10h
|A2    |Història     |Dimecres a les 10h
|===

[IMPORTANT]
====
Sempre que trobem una relació ternària ens hem de plantejar si la mateixa
situació es pot modelar utilitzant dues relacions binàries.
====

En aquest cas, per exemple, podríem plantejar una relació d'aules amb
matèries, i una altra de matèries amb horaris:

image::images/ternary_relationship_as_two_binaries.png["Exemple d'una relació ternària com a dues binàries",scaledwidth="75%"]

Les dades quedarien així:

[options="headers"]
|===
|Aules |Matèries
|A1    |Matemàtiques
|A1    |Història
|A2    |Matemàtiques
|A2    |Història
|===

[options="headers"]
|===
|Matemàtiques |Dilluns a les 9h
|Matemàtiques |Dimecres a les 10h
|Matemàtiques |Dijous a les 9h
|Història     |Dilluns a les 10h
|Història     |Dimecres a les 10h
|===

En aquest exemple, veiem que tot i saber quines matèries s'imparteixen a cada
aula, i en quin horari s'imparteix cada matèria, és impossible saber a quina
aula s'està fent una matèria a una certa hora. Per exemple, no podem saber si
les matemàtiques de dimecres a les 10h es fan a l'aula A1 o a l'aula A2.

Fins i tot afegint una tercera relació, entre aules i horaris, és insuficient:

image::images/ternary_relationship_as_three_binaries.png["Exemple d'una relació ternària com a tres binàries",scaledwidth="75%"]

[options="headers"]
|===
|Aules |Horaris
|A1    |Dilluns a les 9h
|A1    |Dilluns a les 10h
|A1    |Dimecres a les 10h
|A2    |Dimecres a les 10h
|A2    |Dijous a les 9h
|===

Noteu que no podem saber de cap manera si dimecres a les 10h s'està fent
matemàtiques a l'aula A1 i història a l'aula A2, o bé si és al revés.

Per tant, estem en un cas clar de relació ternària.

=== Connectivitat de les relacions ternàries

La connectivitat de les relacions ternàries pot ésser 1:1:1, N:1:1, M:N:1, o
M:N:P.

Per decidir la connectivitat procedim de la següent manera:

- Fixades una assignatura i una aula, es pot impartir en diversos moments. Per
tant, la connectivitat de la banda Horari és _molts_.

- Fixades una assignatura i un determinat horari, es pot impartir en més d'una
aula (per exemple, hi podrien haver diverses classes de matemàtiques de 3r
d'ESO simultànies impartides per professors diferents a aules diferents). Així,
la connectivitat de la banda Aula és _molts_.

- Fixades una aula i un determinat horari, només s'hi pot impartir una
assignatura, llavors la connectivitat de la banda Assignatura és _un_.

.Exemple d'una relació ternària.
image::images/ternary_mapping_relationships.png["Exemple d'una relació ternària",scaledwidth="75%"]

== Tipus d'entitats

=== Entitats fortes i entitats dèbils

Un tipus d'entitat és _forta_ si els seus atributs permeten identificar de forma
única cadascuna de les entitats. La major part d'entitats són d'aquest tipus.
Les entitats fortes poden existir a part de totes les altres entitats que s'hi
relacionen.

En canvi, un tipus d'entitat és _dèbil_ si els seus atributs no la
identifiquen completament, sinó que només la identifiquen parcialment. Una
entitat dèbil necessita participar en una relació per tal que pugui ésser
identificada.

Empleat o departament són exemples d'entitats fortes.

Una línia de factura és un exemple d'entitat dèbil. Necessita relacionar-se amb
la factura a què pertany per tal de poder-se distingir.

De la mateixa manera, si en una base de dades guardem informació sobre diverses
escoles, l'entitat aula necessita de la relació amb escola per poder-se
identificar correctament.

Les entitats dèbils es representen en un diagrama ER utilitzant una línia doble
pel contorn del seu rectangle. La relació que ajuda a identificar-les
completament es representa amb una línia doble.

.Exemple d'entitat dèbil.
image::images/weak_entity.png["Exemple d'entitat dèbil",scaledwidth="50%"]

== Extensions al model entitat-relació

Amb el temps, s'han ampliat els diagrames ER per poder modelar algunes
situacions habituals fàcilment.

=== Generalització / Especialització

Sovint ens trobem amb tipus d'entitats que tenen forces característiques
comunes (atributs i relacions). En molts casos, la mateixa entitat concreta
pot ésser de diversos d'aquests tipus d'entitats.

En aquestes situacions surt a compte agrupar les característiques comunes de
tots els tipus d'entitats relacionats en un tipus, i indicar que els altres
són similars, cosa que ens permet especificar només les seves diferències.

La relació de generalització, molt similar a l'herència de classes en la
programació orientada a objectes, ens permet modelar això en un diagrama ER.

Per exemple, imaginem que a la nostra base de dades tenim dos tipus de persones:
els clients i els empleats. Independentment que una persona sigui un client o
un empleat, en voldrem guardar el nom complet, l'adreça, el DNI, etc. així que
pot tenir sentit agrupar-los en un mateix tipus d'entitat:

.Exemple de generalització
image::images/generalization.png["Exemple de generalització",scaledwidth="100%"]

A les relacions de generalització hem d'indicar si la relació és parcial o
total:

- Total: totes les entitats del tipus general són sempre d'algun dels tipus
especialitzats.

- Parcial: hi ha algunes entitats del tipus general que no són de cap dels
tipus especialitzats.

A més, també indicarem si la relació és disjunta o encavalcada:

- Disjunta: si una entitat pertany a un dels tipus especialitzats, no pot
pertànyer a cap altre dels tipus especialitzats.

- Encavalcada: hi poden haver entitats que pertanyin a més d'un dels tipus
especialitzats.

Per indicar en quina situació ens trobem posarem les inicials corresponents al
costat de la relació. Per exemple, si la relació és total i disjunta posaríem
"T, D".

A l'exemple anterior les lletres que apareixen són T (total) i E (encavalcada).
És total perquè totes les persones que hi ha a la nostra base de dades són
clients o empleats. I és encavalcada perquè podria haver-hi empleats que també
fossin clients.

[WARNING]
====
No sempre que detectem camps repetits volem fer una relació de generalització.

Si el diagrama compleix els nostres requeriments sense necessitat de
recórrer a la generalització, és millor mantenir-lo simple.
====

== Termes claus

*clau alternativa*: totes les claus candidates que no s'han triat com a clau
primària.

*clau candidata*: una clau simple o composta que és única (no poden existir dues
files en una taula que tinguin els mateixos valors) i minimal (cada columna és
necessària).

*atributs univaluats*: són aquells que reben un sol valor per a cada
ocurrència d'una entitat.

*atributs compostos*: atributs que consisteixen en una jerarquia d'atributs.

*clau composta*: clau composta de dos o més atributs, però que ha de ser
minimal.

*entitats dèbils*: aquestes entitats depenen de la relació amb una altra
entitat per definir-se completament.

*atributs derivats*: atributs que contenen valors calculats a partir d'altres
atributs.

*entitat*: un objecte o element del món real amb una existència independent que
pot ésser diferenciada d'altres objectes.

*model de dades entitat relació (ER)*: model de dades adequat per al disseny
de bases de dades, els elements principals del qual són les entitats, les
relacions i els atributs. Es representa mitjançant diagrames ER.

*esquema entitat relació*: veure _model de dades entitat relació_.

*tipus d'entitat*: una col·lecció d'entitats similars.

*entitat forta*: entitat que té una existència per ella mateixa i no necessita
de cap altra entitat per poder identificar unívocament totes les seves
instàncies.

*clau*: un atribut o grup d'atributs els valors dels quals es poden utilitzar
per identificar unívocament una entitat individual en un conjunt d'entitats.

*atributs multivaluats*: atributs que tenen un conjunt de valors per a
cadascuna de les entitats.

*n-ària*: que una relació sigui n-ària significa que hi participen _n_ entitats.

*relacions*: les associacions o interaccions entre entitats; s'utilitzen per
connectar informació relacionada entre les entitats.

*atributs simples*: extrets dels dominis de valors atòmics.

*relació ternària*: un tipus de relació que involucra a tres entitats.

*relació recursiva*: una relació que existeix entre ocurrències del mateix
conjunt d'entitats.

*relació binària*: un tipus de relació que involucra a dues entitats.
