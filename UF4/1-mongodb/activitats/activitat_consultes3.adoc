= MongoDB
:doctype: article
:encoding: utf-8
:lang: ca
:toc: left
:toclevels: 3
:numbered:
:ascii-ids:

<<<

== Activitat consultes 3

Utilitzarem la col·lecció link:https://github.com/mledoze/countries/blob/master/countries.json[countries].

Per importar la base de dades segueix els passos següents:

1. Descarrega el fitxer. Importa'l amb:
+
----
$ mongoimport -d test -c countries --jsonArray <ruta al fitxer countries.json>
----

2. El sistema t'ha d'informar que s'han importat 248 documents.

=== Exercicis

[NOTE]
====
Per a cada exercici es vol:

- La consulta demanada.
- Els resultats obtinguts (o part dels resultats si en retorna molts).
====

1. Cerca en quants països s'utilitza l'euro.

2. Cerca en quines subregions està dividida Europa.

3. Cerca quin país té el domini d'Internet (_tld_) `.tk`.

4. Cerca quins països tenen més d'un domini d'Internet.

5. Cerca quins països tenen una jota al seu nom (majúscula o minúscula).

6. Cerca el nom nadiu del país que en espanyol es diu "Japón".

7. Cerca el nom dels països que tenen més de 5 països veïns.

8. Cerca en quants països es parla francès.

9. Cerca el nom comú i el nom oficial dels països africans en què es parla
francès.

10. Cerca el nom comú i el continent (_region_) dels països no africans en què es parla àrab.

11. Cerca com es diu Espanya en rus.

12. Cerca en quants països existeix més d'una divisa oficial.

13. Cerca el nom dels països que són limítrofs amb Sèrbia.

14. Cerca el nom dels països en què l'anglès és l'única llengua oficial.

15. Cerca quants idiomes es parlen a Espanya.
