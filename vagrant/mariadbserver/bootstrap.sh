#!/bin/sh

# Neteja
apt-get purge -y mariadb-server
apt-get auto-remove -y
rm -R /var/lib/mysql

# Instal·lació mariadb
apt-get update
apt-get install -y mariadb-server

# Importa BD sakila, miniwind, i chinook
wget -nv https://gitlab.com/joanq/DAM-M2-BasesDeDades/raw/master/db/mariadb/sakila_onlytables/sakila-schema.sql
wget -nv https://gitlab.com/joanq/DAM-M2-BasesDeDades/raw/master/db/mariadb/sakila_onlytables/sakila-data.sql
mysql -u root -psuper3 < sakila-schema.sql
mysql -u root -psuper3 < sakila-data.sql
wget -nv https://gitlab.com/joanq/DAM-M2-BasesDeDades/raw/master/db/mariadb/miniwind.sql
mysql -u root -psuper3 < miniwind.sql
wget -nv https://github.com/lerocha/chinook-database/raw/master/ChinookDatabase/DataSources/Chinook_MySql_AutoIncrementPKs.sql
mysql -u root -psuper3 < Chinook_MySql_AutoIncrementPKs.sql

# Configuració de MariaDB:
# - Permet connexions des de qualsevol host
# - Activa GROUP BY estricte
# - Permet || com a CONCAT (PIPES_AS_CONCAT)
# - No permet " com a delimitador de cadenes, només ' (ANSI_QUOTES)
cp /vagrant/50-server.cnf /etc/mysql/mariadb.conf.d

# Crea l'usuari admin amb accés remot
mysql << EOF
CREATE USER admin@'%' IDENTIFIED BY 'super3';
GRANT ALL ON *.* TO admin@'%';
EOF
systemctl restart mariadb
