#!/bin/sh

# Neteja
debconf-set-selections << EOF
postgresql-11	postgresql-11/postrm_purge_data	boolean	true
EOF
apt-get purge -y postgresql postgresql-11

# Instal·lació postgresql
apt-get update
apt-get install -y postgresql

# Crea usuari gestor
su - postgres -c "createuser -d -r -P gestor" <<EOF
super3
super3
EOF

# Importa BD pagila
wget -nc -nv https://gitlab.com/joanq/DAM-M2-BasesDeDades/raw/master/db/postgresql/pagila/pagila-schema.sql
wget -nc -nv https://gitlab.com/joanq/DAM-M2-BasesDeDades/raw/master/db/postgresql/pagila/pagila-data.sql
sed -i s/"OWNER TO postgres"/"OWNER TO gestor"/g pagila-schema.sql
su - postgres -c "createdb pagila --owner=gestor"
su - postgres -c "psql -d pagila" <<EOF
\i /home/vagrant/pagila-schema.sql
\i /home/vagrant/pagila-data.sql
EOF

# Importa BD world
wget -nc -nv https://gitlab.com/joanq/DAM-M2-BasesDeDades/raw/master/db/postgresql/worldutf8.sql
psql -h 127.0.0.1 -U gestor -d postgres <<EOF
super3
create database world;
\c world
\i worldutf8.sql
EOF

# Permet connexions des del host
sed -i s/"127.0.0.1\/32"/"0.0.0.0\/0"/ /etc/postgresql/11/main/pg_hba.conf
sed -i s/"^#listen_addresses = 'localhost'"/"listen_addresses = '*'"/ /etc/postgresql/11/main/postgresql.conf
systemctl restart postgresql
